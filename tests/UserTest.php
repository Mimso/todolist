<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

use App\Entity\User;

class UserTest extends KernelTestCase
{
    public function testUserCheckEmailValid(): void
    {
        $user = new User();
        $user->setEmail("test-esgi@gmail.com");
        $this->assertTrue($user->checkEmail());
    }

    public function testUserCheckEmailInvalid(): void
    {
        $user = new User();
        $user->setEmail("test-esgigmail.com");
        $this->assertFalse($user->checkEmail());
    }

    public function testUserCheckFirstnameValid(): void
    {
        $user = new User();
        $user->setFirstname("Yves");
        $this->assertTrue($user->hasFirstname());
    }

    public function testUserCheckFirstnameInvalid(): void
    {
        $user = new User();
        $user->setFirstname("");
        $this->assertFalse($user->hasFirstname());
    }

    public function testUserCheckLastnameValid(): void
    {
        $user = new User();
        $user->setLastname("Skrzypczyk");
        $this->assertTrue($user->hasLastname());
    }

    public function testUserCheckLastnameInvalid(): void
    {
        $user = new User();
        $user->setLastname("");
        $this->assertFalse($user->hasLastname());
    }

    public function testUserCheckPasswordValidBetween(): void
    {
        $user = new User();
        // 16 characters
        $user->setPassword("6i6qqust9tfz4p74");
        $this->assertTrue($user->checkPassword());
    }

    public function testUserCheckPasswordValidHigh(): void
    {
        $user = new User();
        // 40 characters
        $user->setPassword("ndt4l2vfdsqtwj4gm9q5klg31017h0vdbgfqigyl");
        $this->assertTrue($user->checkPassword());
    }

    public function testUserCheckPasswordValidLow(): void
    {
        $user = new User();
        // 8 characters
        $user->setPassword("84t2msd1");
        $this->assertTrue($user->checkPassword());
    }

    public function testUserCheckPasswordInvalidLess(): void
    {
        $user = new User();
        // 6 characters
        $user->setPassword("ppp7um");
        $this->assertFalse($user->checkPassword());
    }

    public function testUserCheckPasswordInvalidMore(): void
    {
        $user = new User();
        // 45 characters
        $user->setPassword("z44pjpp77wrg03t8rivad00ru7t9btpgcjjn1i3suv02i");
        $this->assertFalse($user->checkPassword());
    }

    public function testUserCheckIsValid() {
        $user = new User();
        $user->setFirstname("Yves");
        $user->setLastname("Skrzypczyk");
        $user->setEmail("yves-skrzypczyk@esgi.fr");
        $user->setPassword("6i6qqust9tfz4p74");
        $this->assertTrue($user->isValid());
    }
}
