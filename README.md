# TodoList

[![license](https://img.shields.io/badge/Made%20with-%E2%9D%A4%EF%B8%8F-FFC0CB.svg?style=for-the-badge)]()

### Build with
[![PHP](https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white)](https://php.net) [![Symfony](https://img.shields.io/badge/symfony-%23000000.svg?style=for-the-badge&logo=symfony&logoColor=white)](https://symfony.com) [![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)](https://postgresql.org) [![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)](https://docker.com)

## Description

The TodoList is a list of tasks to accomplish. These tasks can be independent of each other and concern daily life.

## Installation

TodoList requires to run:
- [PHP](https://php.net/) 8.0+
- [Composer](https://composer.org)
- [Docker with Docker compose](https://docker.com)

Clone the repo from github.
```sh
git clone https://gitlab.com/Mimso/todolist.git
```

Install the dependencies and start docker compose.

```sh
make install
```
or
```sh
composer install
docker-compose up -d
```
## Tests

To run all the PhpUnits test, simply run the command :

```sh
make test
```
or
```sh
php vendor/bin/phpunit tests
```

## License

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg?style=for-the-badge)](https://www.gnu.org/licenses/gpl-3.0)
