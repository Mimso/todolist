.PHONY: up
up: ## Start all docker container in daemon mode
	docker-compose up -d

.PHONY: down
down: ## Stop all docker container
	docker-compose down

.PHONY: restart
restart: ## Restart all docker container in daemon mode
	docker-compose down && docker-compose up -d

.PHONY: install
test: ## test
	composer install
    docker-compose up -d

.PHONY: test
test: ## test
	php vendor/bin/phpunit tests