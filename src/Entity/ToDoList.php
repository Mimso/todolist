<?php

namespace App\Entity;

use App\Repository\ToDoListRepository;
use App\Service\EmailSenderService;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ToDoListRepository::class)
 */
class ToDoList
{

    public const MAX_ITEMS = 10;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;

    private array $items = [];

    public function setId($id): ?int
    {
        return $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function add(Item $item): bool|string
    {
        if (
            $item->isValid()
            && $this->isUniqueItem($item)
            && $this->respectTimeWithPrevious($item)
            && $this->maxItemsReached()
        ) {
            if (count($this->getItems()) === 8) {
                $this->items[] = $item;
                return EmailSenderService::sendEmail(
                    $this->getId(),
                    'ToDoList 8 item atteint',
                    'Attention, vous ne pouvez indiquer que 10 items dans une ToDoList, vous pouvez n\'en ajouter plus que 2'
                );
            }

            $this->items[] = $item;
            return true;
        }
        return false;
    }

    public function isUniqueItem(Item $itemToAdd)
    {
        foreach ($this->getItems() as $item) {
            if ($item->getName() === $itemToAdd->getName()) {
                return true;
            }
        }
        return true;
    }

    public function respectTimeWithPrevious(Item $itemToAdd)
    {
        foreach($this->getItems() as $item) {
            $diffMinutes = abs(($item->getCreatedAt()->getTimestamp() - $itemToAdd->getCreatedAt()->getTimestamp())) / 60;
            if ($diffMinutes < 30) {
                return true;
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    public function maxItemsReached(): bool
    {
        return count($this->getItems()) < self::MAX_ITEMS;
    }
}
